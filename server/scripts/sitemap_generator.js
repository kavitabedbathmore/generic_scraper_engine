//var Crawler = require("js-crawler");
// new Crawler().configure({depth: 3})
//   .crawl("http://www.flipkart.com", function onSuccess(page) {
//     console.log(page.url);
//   });

	var ineed = require('ineed');
	var async = require('async');
	var fs = require('fs');
	var json2csv = require('json2csv');
	var urls = []; var fields = ["href", "text"];

	var blackListedItems = ["STORES", "SHIP TO", "New", "Christmas", "Hanukkah", "New Year\'s Eve", "Gifts", "Toy Shop", "Blog", "Ideas & Advice", "Wedding Registry", "Sign In", "Sign Up", "Registry Benefits", "Registry Bonus Gifts", "Registry Checklist", "Start Now", "Top 6 Reasons to Register", "Private Registry Events", "Sign in","View Cart","Checkout","Site Index","Terms of Use","Privacy Policy", "View Desktop Site", "Customer Service", "Careers", "Feedback", "Returns/Exchanges", "Customer Service", "Order Tracking", "About Us", "Contact Us", "Free", "All", "Special Offers", "off", "Holiday", "Limited Time Offers", "Rated", "Top", "Use", "How to", "Top Rated Outdoor", "Sale", "New Clearance", "Free Shipping", "All Free Shipping"];


ineed.collect.hyperlinks.from('http://www.crateandbarrel.com/',
  function(err, response, result) {
    // console.log(result.hyperlinks.length);
    var hyperlinks = result.hyperlinks;
    if (hyperlinks && typeof hyperlinks !== 'undefined' && hyperlinks instanceof Array && hyperlinks.length > 0) {

      hyperlinks.forEach(function(item, index) {
        item.text = item.text.replace(/\n/g, "").trim();
				if( item.text && (blackListedItems.indexOf(item.text) == -1) ){
					if( item.text.indexOf("All") == -1 || item.text.indexOf("Free") == -1  || item.text.indexOf("%") == -1 || item.text.indexOf("Offers") == -1 || item.text.indexOf("Sale") == -1){
						urls.push(item);
					}
				}
      });

    	// console.log(urls.length);
    	// console.log(urls);

			if(urls.length > 0){
				json2csv({
					data: urls,
					fields: fields
				}, function(csvErr, csv) {
					if (csvErr) {
						console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
						console.log("array to csv conversion error: " + csvErr);
						console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
						// return done(csvErr);
					} else {
						var milliseconds = new Date().getTime();
						fs.writeFile('./server/resources/' + "new_scraper" + "_" + milliseconds + '.csv', csv, function(fileWriteErr) {
							if (fileWriteErr) {
								console.log("File write error:" + fileWriteErr);
								// return done(fileWriteErr);
							} else {
								console.log('file saved');
								console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
								console.log("All ok!");
								console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
								// return done(null);
							}
						});
					}
				});
			}else{
				console.log("urls not found");
			}

    } else {
      console.log("urls not found");
    }
  });
