var async = require('async');
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var parseString = require('xml2js').parseString;
var Converter = require("csvtojson").Converter;
var converter = new Converter({});
var json2csv = require('json2csv');
var path = require("path");
var superagent = require('superagent');
var fields = ['title'];

var models = require('../../server/server.js').models;
var JustdialServiceProvider = models.justdial_service_provider;
// srchpagination
// https://www.justdial.com/Mumbai/Plumbers/ct-134859/page-50

// var cities = ["Ahmedabad", "Bangalore", "Chandigarh", "Chennai", "Coimbatore", "Delhi", "Goa", "Gurgaon", "Hyderabad", "Indore", "Jaipur", "Kolkata", "Mumbai", "Noida", "Pune"]
var cities = ["Mumbai"]

module.exports.justdial_scraper = function(callback) {

  var skip_records = 0;
  var insertCount = 0;
  var failedInsertCount = 0;
  var find_response = {};
  async.whilst(function() {
      console.log("inside whilst skip_records =" + skip_records);
      if (Object.keys(find_response).length > 0 || skip_records === 0) {
        return true;
      } else {
        return false;
      }
    },
    function(next) {
      find_response = {};
      JustdialServiceProvider.find({
        where: {
          // catCategory: 'Candles & Holders' // change it accordingly
          // catCategory : { inq: [ "Acrylic", "Tobacciana", "Blankets & Throws", "Linens", "Art Objects" ] }
          // catCategory: { inq: ["Blankets & Throws", "Drink & Barware", "Baskets & Bowls", "Baskets", "Bath Bombs", "Bath Oils"] }
        },
        limit: 5,
        skip: skip_records
      }, function(err, response) {
        if (err) {
          console.log("Affiliate products query error : " + err);
          return callback(err);
        } else {
          if (response) {
            find_response = response;
            async.eachSeries(response, function(record, recordCallback) {
console.log(record.detailPageUrl);

							if(record.detailPageUrl){
								superagent
								.get(record.detailPageUrl)
								.set('Accept', 'application/json')
								.end(function(err, res){
									if(err){
										return setImmediate(recordCallback);
									}else{
										var $ = cheerio.load(res.text);
										$("body").children("div[id='setbackfix']").filter(function() {

											record.detailPageBannerImg = $(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class","row dtlwpr").children("div").attr("class","col-sm-12 padding0 col-xs-12").children("div[class='detail-banner']").children("img[id='display_pic']").attr("src");

											record.detailPageTitle = $(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class","row dtlwpr").children("div").attr("class", "company-details").next().children("div").attr("class","col-sm-9 col-xs-12").children("div[class='tleorlp']").children("h1[class='rstotle']").text().replace(/\n/g,"").replace(/\t/g,"").trim();

											record.detailPageRating = $(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class","row dtlwpr").children("div").attr("class", "company-details").next().children("div").attr("class","col-sm-9 col-xs-12").children("div[class='tleorlp']").children("div").children("span[class='strtngwpr']").children("span[class='rating']").children("span[class='total-rate']").children("span").prop("itemprop","ratingValue").text().trim();

											record.detailPagenoOfVotes = $(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class","row dtlwpr").children("div").attr("class", "company-details").next().children("div").attr("class","col-sm-9 col-xs-12").children("div[class='tleorlp']").children("div").children("span[class='strtngwpr']").children("span[class='rtngsval']").children("span[class='votes']").text().trim();

											record.detailPageTelephone = $(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class", "col-sm-12 col-xs-12 dtpage").next().next().next().children("div").attr("class","col-sm-4 col-xs-12 padding0 leftdt").children("div").attr("class", "col-sm-12 col-xs-12 padding0 paddingR0").children("ul[class='comp-contact']").children("li").first().children("span").attr("itemprop", "telephone").children("div").attr("class", "telCntct cmawht").children("a").children("b").text().replace(/\n/g,"").replace(/\t/g,"").trim();

											record.detailPageTelephone2 = $(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class", "col-sm-12 col-xs-12 dtpage").next().next().next().children("div").attr("class","col-sm-4 col-xs-12 padding0 leftdt").children("div").attr("class", "col-sm-12 col-xs-12 padding0 paddingR0").children("ul[class='comp-contact']").children("li").next().children("span").attr("itemprop", "telephone").children("a[class='tel']").text().replace(/\n/g,"").replace(/\t/g,"").trim();

											record.detailPageAddress =  $(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class", "col-sm-12 col-xs-12 dtpage").next().next().next().children("div").attr("class","col-sm-4 col-xs-12 padding0 leftdt").children("div").attr("class", "col-sm-12 col-xs-12 padding0 paddingR0").children("ul[class='comp-contact']").children("li").next().next().children("span[class='comp-text']").children("span[id='fulladdress']").children("span").attr("itemprop", "address").children("span").attr("itemprop","streetAddress").text().replace(/\n/g,"").replace(/\t/g,"").trim();




// console.log($(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class", "col-sm-12 col-xs-12 dtpage").next().next().next().children("div").attr("class","col-sm-4 col-xs-12 padding0 leftdt").children("div").attr("class", "col-sm-12 col-xs-12 padding0 paddingR0").children("div[id='gal_img']").children("ul[class='catyimgul']").length);

// if($(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class", "col-sm-12 col-xs-12 dtpage").next().next().next().children("div").attr("class","col-sm-4 col-xs-12 padding0 leftdt").children("div").attr("class", "col-sm-12 col-xs-12 padding0 paddingR0").children("div[id='gal_img']").children("ul[class='catyimgul']").length > 0){
//
// $(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class", "col-sm-12 col-xs-12 dtpage").next().next().next().children("div").attr("class","col-sm-4 col-xs-12 padding0 leftdt").children("div").attr("class", "col-sm-12 col-xs-12 padding0 paddingR0").children("div[id='gal_img']").children("ul[class='catyimgul']").children("li").each(function(i, elem){
//
// 	console.log($(this).data("original"));
//
// });
// process.exit(1);
// }




if(!record.detailPageAddress || !record.detailPageTelephone){
	console.log("--------------------------------------------- INSIDE IFFF");
	$(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class", "col-sm-12 col-xs-12 dtpage").next().next().next().children("div").attr("class","col-sm-4 col-xs-12 padding0 leftdt").children("div").attr("class", "col-sm-12 col-xs-12 padding0 paddingR0").children("ul[class='comp-contact']").children("li").each(function(i, elem) {

		var address = $(this).children("span[class='comp-text']").text();
		var telephone = $(this).children("span").attr("itemprop","telephone").children("div").attr("class", "telCntct cmawht").text();

		var telephone2 = $(this).children("span").attr("itemprop","telephone").children("div").attr("class", "telCntct cmawht").text();

		if(address){
			record.detailPageAddress = $(this).children("span[class='comp-text']").text().replace(/\n/g,"").replace(/\t/g,"").replace(/\(Map\)/g,"").trim();
		}

		if(telephone){
			record.detailPageTelephone = telephone;
		}
		console.log($(this).children("span[itemprop='telephone']").length);
	});
	// console.log(record.detailPageAddress);
}

console.log("======================================================");
console.log(record);
console.log("======================================================");



											// record.detailPageServiceProviderSite = $(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class", "col-sm-12 col-xs-12 dtpage").next().next().next().children("div").attr("class","col-sm-4 col-xs-12 padding0 leftdt").children("div").attr("class", "col-sm-12 col-xs-12 padding0 paddingR0").children("ul[class='comp-contact']").children("li").last().children("span").attr("class", "mreinfp comp-text").children("a").text().replace(/\n/g,"").replace(/\t/g,"").trim();

											// record.detailPageServiceProviderSiteUrl = $(this).children("div").first().children("div[class='mndtlwpr']").children("div").attr("class", "col-sm-12 col-xs-12 dtpage").next().next().next().children("div").attr("class","col-sm-4 col-xs-12 padding0 leftdt").children("div").attr("class", "col-sm-12 col-xs-12 padding0 paddingR0").children("ul[class='comp-contact']").children("li").last().children("span").attr("class", "mreinfp comp-text").children("a").attr("href");

											record.save(function(err,res){
												if(err){
													console.log(err);
												}else{
													console.log(res);
												}
											});

										});
										return setImmediate(recordCallback);
									}
								});

							}else{
								return setImmediate(recordCallback);
							}

            }, function(recordErr, recordRes) {
              if (recordErr) {
                console.log("Affiliate products async error : " + recordErr);
                return next();
              } else {
                skip_records = skip_records + 5;
                console.log("skip_records is = " + skip_records);
                console.log("Loop ended successfully");
                return next();
              }
            });
          } else {
            console.log("Query response is empty");
            return callback(new Error("affiliate products are not present in the database"));
          }
        }
      });
    },
    function(err) {
      if (err) {
        console.log("Whilist error : " + err);
        return callback(err);
      } else {
        console.log("All the products are done!");
        console.log("Total records inserted : " + insertCount);
        console.log("Total Records failed to insert : " + failedInsertCount);
        return setImmediate(callback);
      }
    });
};


module.exports.justdial_scraper(function(err, res) {
  if (err) {
    console.log(err);
    process.exit(1);
  } else {
    console.log("------------------------------DONE WITH SCRAPING------------------------------");
    process.exit(1);
  }
});
