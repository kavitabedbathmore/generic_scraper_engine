var SitemapGenerator = require('sitemap-generator');

var generator = new SitemapGenerator('https://www.flipkart.com');
console.log("Inside the file");

generator.on('done', function(sitemap) {
	var fields = ['data_urls'];
	var siteMapUrlsFetched = [];

	sitemap.forEach(function(item,index,array){
		siteMapUrlsFetched.push({"data_urls": item});
	});

  json2csv({
    data: siteMapUrlsFetched,
    fields: fields
  }, function(csvErr, csv) {
    if (csvErr) {
      console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
      console.log("array to csv conversion error: " + csvErr);
      console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    } else {
      fs.writeFile("./server/resources/" + 'test.csv', csv, function(fileWriteErr) {
        if (fileWriteErr) {
          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
          console.log("File write error:" + fileWriteErr);
          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
        } else {
          console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
          console.log('test.csv file saved' + "@ " );
          console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        }
      });
    }
  });

});

generator.start();
console.log("start called");
