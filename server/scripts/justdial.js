var async = require('async');
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var parseString = require('xml2js').parseString;
var Converter = require("csvtojson").Converter;
var converter = new Converter({});
var json2csv = require('json2csv');
var path = require("path");
var superagent = require('superagent');

var fields = ['title', 'shop_link', 'shop_name', 'item_details', 'currency', 'price', 'currencySymbol', 'currencyValue', 'overview', 'images', 'main_category', 'mid_category', 'cat_category', 'l4_category', 'l5_category', 'data_listing_id', 'listing_title', 'detail_page_url', 'url_called'];

var models = require('../../server/server.js').models;
var Justdial = models.justdial;



var insertedCount = 0;
var insertedFailedCount = 0;
var priceNotPresentCount = 0;
var dataListingIdNotPresentCount = 0;

var cityUrls = ["https://www.justdial.com/Shop-Online?city=Ahmedabad",
  "https://www.justdial.com/Shop-Online?city=Bangalore",
  "https://www.justdial.com/Shop-Online?city=Chandigarh",
  "https://www.justdial.com/Shop-Online?city=Chennai",
  "https://www.justdial.com/Shop-Online?city=Coimbatore",
  "https://www.justdial.com/Shop-Online?city=Delhi",
  "https://www.justdial.com/Shop-Online?city=Goa",
  "https://www.justdial.com/Shop-Online?city=Gurgaon",
  "https://www.justdial.com/Shop-Online?city=Hyderabad",
  "https://www.justdial.com/Shop-Online?city=Indore",
  "https://www.justdial.com/Shop-Online?city=Jaipur",
  "https://www.justdial.com/Shop-Online?city=Kolkata",
  "https://www.justdial.com/Shop-Online?city=Mumbai",
  "https://www.justdial.com/Shop-Online?city=Noida",
  "https://www.justdial.com/Shop-Online?city=Pune"
];

var cities = ["Ahmedabad", "Bangalore", "Chandigarh", "Chennai", "Coimbatore", "Delhi", "Goa", "Gurgaon", "Hyderabad", "Indore", "Jaipur", "Kolkata", "Mumbai", "Noida", "Pune"]

module.exports.justdial_scraper = function(done) {

	// // var reqUrl = "https://www.justdial.com/Mumbai/Electrical-Accessories/cid-1582/page-1";
	// var reqUrl = "https://www.justdial.com/Shop-Online?city=Mumbai";
	// // var reqUrl = "https://www.justdial.com/Mumbai/Electrical-Accessories/cid-1582/page-0?sort=pop_desc&fsearch=switches";

	var productList = [];
	var totalPages = 0;
	var page = 0;


	var host = 'https://www.justdial.com';
	var category = 'Electrical-Accessories/cid-1582';
	// var query_str = "?sort=pop_desc";
	// query_str = query_str + "&fsearch=switches";

// var reqUrl = "https://www.justdial.com/Mumbai/Electrical-Accessories/cid-1582/page-0?sort=pop_desc&fsearch=switches";

async.eachSeries(cities, function(city, cityCallback) {

var cityUrl = host + "/" + city;
cityUrl = cityUrl + "/" + category;
console.log("Inside city  ==  " + city);
	async.whilst(function() {
			console.log("inside whilst page =" + page);
			if (page == 0 || page < totalPages) {
				return true;
			} else {
				return false;
			}
		},
		function(next) {

			var reqUrl = cityUrl + "/" + "page-" + page;
			// reqUrl = reqUrl + query_str;
			console.log(reqUrl);
			console.log("PAGE---------------------------------------------------------------- ", page);

			//  https://www.justdial.com/Mumbai/Electrical-Accessories/cid-1582/page-0?sort=pop_desc&fsearch=switches

				page++;
				superagent
				.get(reqUrl)
				.set('Accept', 'application/json')
				.end(function(err, res){

					var $ = cheerio.load(res.text);
					totalPages = $("#total").val(); // page number starts with ZERO and less than the 1 unit of total count
					var product = {};
					console.log("inside request");

					$("body").children("article").attr("class","shopwrp").children("section").attr("class","shopinner").children("section").attr("class","shop").children("div[class='shopshowcase']").children("div[class='shwrm']").filter(function() {

								$(this).children("div[class='shopbox']").each(function(i, elem) {

									product.city = city;
									product.keyword = category;
									product.detailPageUrl = $(this).children("a").attr("href");
									product.title = $(this).children("a").attr("title");
									product.listingImage = $(this).children("a").children("div[class='shopboxinner']").children("div[class='sbox_img']").children("div[class='img_wrp']").children("img").attr("src");
									product.listingPrice = $(this).children("a").children("div[class='shopboxinner']").children("div[class='sbox_ft']").children("span[class='ft-price']").children("span[class='dt_price']").text();

									if(product.detailPageUrl && product.detailPageUrl.indexOf("pid-") > -1){

										var pid = product.detailPageUrl.split("pid-")[1];
										if(pid.indexOf("?") > -1){
											product.dataListingId = pid.split("?")[0];
											console.log(product);
											// productList.push(product);
											Justdial.create(product, function(errObj,resObj){
												if(errObj){
													console.log("Product insertion error :", errObj);
												}else{
													console.log("Product inserted successfully");
												}
											});
										}
									}
								});

							});
							return next();
				});
		},
		function(err) {
			if (err) {
				console.log("Pagination error : " + err);
				return setImmediate(cityCallback);
			} else {
				console.log("All the pages are traversed successfully");
				return setImmediate(cityCallback);
			}
		});
}, function(recordErr, recordRes) {
	if (recordErr) {
		console.log("City async error : " + recordErr);
		return setImmediate(done);
	} else {
		console.log("All the cities are done");
		// console.log("Loop ended successfully");
		return setImmediate(done);
	}
});







	/*
  async.eachSeries(fileJsonArray, function(fileData, fileDataCallback) {
    console.log(fileData.cat_category);

    if (fileData.detail_page_url && fileData.detail_page_url !== "no-data") {

      request(fileData.detail_page_url, function(pageItemErr, pageItemRes, pageItemBody) {
        if (!pageItemErr && pageItemRes.statusCode == 200 && pageItemBody) {
          var $ = cheerio.load(pageItemBody);
          console.log("inside request");
          var product = {};
          product.title = $("#listing-page-cart-inner").children("h1").children("span").text();
          product.shopLink = $("#seller-wrapper").children("div").attr("id", "seller").children("div").next().children("div").children("a").attr("href");
          product.shopName = $("#seller-wrapper").children("div").attr("id", "seller").children("div").next().children("div").children("a").text();

          product.itemDetails = $("#description").children("div").attr("id", "description-text").html();

          product.currency = $(".price-block").children("meta").prop("itemprop", "currency").prop("content");
          product.price = $(".price-block").children("meta").next().prop("itemprop", "price").prop("content");


          if ($("#listing-price").children("span").length > 1) {
            product.currencySymbol = $("#listing-price").children("span").first().text();
            product.currencyValue = $("#listing-price").children("span").last().text();
          } else {
            product.currencySymbol = $("#listing-price").children("span").attr("class", "item-amount currency-text").children("span").first().html();
            product.currencyValue = $("#listing-price").children("span").attr("class", "item-amount currency-text").children("span").last().html();
            // US$ 359.00
          }

          console.log("-----------------------------------------------------------------------");
          console.log(product.currency);
          console.log(product.price);
          console.log(fileData.detail_page_url);
          console.log(product.currencySymbol);
          console.log(product.currencyValue);
          console.log("-----------------------------------------------------------------------");

          if (!product.currencySymbol) {
            console.log("inside.........condition");
            if (fileData.listing_currency && fileData.listing_currency !== '' && fileData.listing_currency !== undefined) {
              product.currencySymbol = fileData.listing_currency;
            } else {
              product.currencySymbol = "";
            }
          }

          if (!product.currencyValue) {
            console.log("inside.........condition");
            if (fileData.listing_price && fileData.listing_price !== '' && fileData.listing_price !== undefined) {
              product.currencyValue = fileData.listing_price;
            } else {
              product.currencyValue = "";
            }
          }

          if ((product.currency && product.price) || (product.currencySymbol && product.currencyValue)) {
            // make it work

            var overview = [];
            product.gmb = '';
            $("#item-overview").children("ul").children("li").each(function(i, elem) {
              overview.push($(this).text());
              if ($(this).text().indexOf("Ships worldwide from ") !== -1) {
                product.gmb = $(this).text().split("Ships worldwide from ")[1];
              } else {
                if (!product.gmb) {
                  if ($(this).text().indexOf("Ships from ") !== -1) {
                    var gmbText = $(this).text().split("Ships from ")[1];
                    if ($(this).text().indexOf(" to select countries.") !== -1) {
                      gmbText = gmbText.split(" to select countries.")[0];
                      product.gmb = gmbText;
                    } else {
                      product.gmb = gmbText;
                    }
                  }
                }
              }
            });

            product.overview = overview;
            var images = [];
            $("#image-main").children("ul").children("li").each(function(i, elem) {
              var imgObj = {};
              imgObj.fullImage = $(this).data("full-image-href");
              imgObj.largeImage = $(this).data("large-image-href");
              images.push(imgObj);
            });
            product.images = images;

            product.dataListingId = fileData.data_listing_id;
            product.listingTitle = fileData.listing_title;
            product.mainCategory = fileData.main_category;
            product.midCategory = fileData.mid_category;
            product.catCategory = fileData.cat_category;
            product.l4_category = fileData.l4_category;
            product.l5_category = fileData.l5_category;
            product.detailPageUrl = fileData.detail_page_url;
            product.urlCalled = fileData.url_called;
            // productList.push(product);
            product.availability = $("div").attr("id", "item-stock").children("meta").prop("item-stock", "availability").prop("content");

            // var internalDivs = $("#variations").children("div").attr("class", "item-variation-options clear").children("div").attr("class", "item-variation-options clear").length;
            var internalDivs = $("div").prop("itemprop", "offerDetails").children('div').attr("id", "variations").children('div[class="item-variation-options clear"]').children('div[class="item-variation-option clear"]').length;

            internalDivs = internalDivs + 1; // fix for above code

            if ((internalDivs - 1) > 0) {

              var divSize = (internalDivs - 1);

              // var internalDivReference = $("#variations").children("div").attr("class", "item-variation-options clear").children("div").attr("class", "item-variation-options clear").first();

              var internalDivReference = $("div").prop("itemprop", "offerDetails").children('div').attr("id", "variations").children('div[class="item-variation-options clear"]').children('div[class="item-variation-option clear"]').first();

              var variants = [];
              while (divSize > 0) {
                var variantObj = {};
                var label = internalDivReference.children("label").text();
                if (label) {

                  variantObj.label = internalDivReference.children("label").text();
                  var variantClass = internalDivReference.children("label").attr("class");
                  var variantPropertyId = variantClass.split("-")[1];

                  var variantData = [];
                  internalDivReference.children("select").data("property-id", variantPropertyId).children("option").each(function(i, elem) {
                    if ($(this).val()) {
                      var obj = {};
                      obj.optionValue = $(this).val();
                      obj.optionText = $(this).text().trim();
                      variantData.push(obj);
                    }
                  });
                  variantObj.data = variantData;
                  variants.push(variantObj);

                  if (divSize === (internalDivs - 1)) {
                    // internalDivReference = $("#variations").children("div").attr("class", "item-variation-options clear").children("div").attr("class", "item-variation-options clear").next();

                    internalDivReference = $("div").prop("itemprop", "offerDetails").children('div').attr("id", "variations").children('div[class="item-variation-options clear"]').children('div[class="item-variation-option clear"]').next();
                  } else {
                    internalDivReference = internalDivReference + ".next()";
                  }
                  divSize--;
                } else {
                  divSize = 0;
                }
              }
              product.variants = variants;
            } else {
              product.variants = [];
            }

            console.log(product.variants);

            if (product.dataListingId) {

              var reviewUrl = "https://www.etsy.com/listing/" + product.dataListingId + "/stubs/feedback";
              var shipingPolicyUrl = "https://www.etsy.com/listing/" + product.dataListingId + "/stubs/policy";

              request(reviewUrl, function(reviewErr, reviewRes, reviewBody) {
                if (!reviewErr && reviewRes.statusCode == 200 && reviewBody) {
                  var $ = cheerio.load(JSON.parse(reviewBody).html);
                  $.html();

                  var parsedHTML = $.load(JSON.parse(reviewBody).html);
                  var reviews = [];
                  parsedHTML("div").attr("class", "feedback-row reviews2 clearfix").each(function(i, elem) {
                    var reviewObj = {};
                    reviewObj.reviewerName = $(this).children("div").attr("class", "col col2").children("div").children("p").attr("class", "feedback-reviewer").children("a").text();

                    if (reviewObj.reviewerName) {

                      reviewObj.reviewerImg = $(this).children("div").attr("class", "col col2").children("div").children("a").first().children('img[class="feedback-image"]').attr("src");

                      reviewObj.reviewDate = $(this).children("div").attr("class", "col col5 col-last comment").children("p").first().attr("class", "feedback-date").text();
                      reviewObj.reviewComment = $(this).children("div").attr("class", "col col5 col-last comment").children("p").last().attr("class", "feedback-comment").text();

                      reviewObj.reviewCommentImgUrl = '';
                      if (!reviewObj.reviewComment) {
                        var thumbImg = $(this).children("div").attr("class", "col col5 col-last comment").children("div").attr("class", "clearfix").children('img[class="appreciation-photo--thumb"]').attr("src");
                        if (thumbImg) {
                          reviewObj.reviewCommentImgUrl = thumbImg;
                        }
                      }

                      reviewObj.reviewTitleImg = $(this).children("div").attr("class", "col col5 col-last comment").children("a").first().children('img[class="feedback-image"]').attr("src");

                      reviewObj.reviewTitle = $(this).children("div").attr("class", "col col5 col-last comment").children("a").last().attr("class", "feedback-title").text();

                      reviewObj.reviewInitialRating = $(this).children("div").attr("class", "col col5 col-last comment").children("div").attr("class", "stars small").children('input[name="initial-rating"]').val();

                      reviewObj.reviewRating = $(this).children("div").attr("class", "col col5 col-last comment").children("div").attr("class", "stars small").children('input[name="rating"]').val();

                      reviews.push(reviewObj);
                    }
                  });

                  product.reviews = reviews;
                  // IF case of review ended...... SHIPPING STARTS HERE
                  request(shipingPolicyUrl, function(shipingPolicyErr, shipingPolicyRes, shipingPolicyBody) {
                    if (!shipingPolicyErr && shipingPolicyRes.statusCode == 200 && shipingPolicyBody) {
                      var $ = cheerio.load(shipingPolicyBody);
                      $.html();

                      var parsedHTML = $.load(JSON.parse(shipingPolicyBody).html);

                      // console.log(parsedHTML("div").children("div").data("region", "policy-subregions"));

                      // console.log(parsedHTML("div").children("div").data("region", "policy-subregions").children('div[class="col-group shop-policies-section"]').length);

                      if (parsedHTML("div").children("div").data("region", "policy-subregions") && parsedHTML("div").children("div").data("region", "policy-subregions").children('div[class="col-group shop-policies-section"]').length > 0) {

                        var shipingPolicy = [];

                        parsedHTML("div").children("div").data("region", "policy-subregions").children('div[class="col-group shop-policies-section"]').each(function(i, elem) {


                          var shippingLabel = $(this).children('div[class="col-lg-3 pt-xs-4 pt-lg-3 pl-xs-1 pb-xs-2 pb-lg-6"]').children('span[class="b"]').text();

                          // var shippingContent =
                          // console.log($(this).children('div[class="col-lg-9 pt-lg-3 pb-xs-2 pl-xs-1"]'));

                          // console.log($(this).children('div[class="col-lg-9 pt-lg-3 pb-xs-2 pl-xs-1"]').children('div[class="pl-xs-0 mb-xs-3"]').children('div').attr("id","listing-shipping-estimate").html());


                          // console.log($(this).children('div[class="col-lg-9 pt-lg-3 pb-xs-2 pl-xs-1"]').children('div[class="pl-xs-0 mb-xs-3"]').children('div').attr("id", "listing-shipping-estimate").html());

                          // pl-xs-0 text-smaller text-gray-lightest
                          // pl-xs-0 mb-xs-3
                          // listing-shipping-estimate

                          // console.log($(this).children('div[class="col-lg-9 pt-lg-3 pb-xs-2 pl-xs-1"]'));
                        });
                        product.shipingPolicy = shipingPolicy;

                        Product.create(product, function(productErr, productObj) {
                          if (productErr) {
                            insertedFailedCount++;
                            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            console.log("Product insertion error : " + productErr);
                            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            return setImmediate(fileDataCallback);
                          } else {
                            insertedCount++;
                            return setImmediate(fileDataCallback);
                          }
                        });

                      } else {
                        var shipingPolicyArr = [];
                        console.log("Its ELSEEEEEEEEEEEEE..............");
                        parsedHTML("div").each(function(i, elem) {
                          var shipping = {};
                          var slabel = $(this).children('div[class="col col2"]').children("h2").text();
                          if (slabel) {
                            shipping.label = slabel;
                            shipping.labelContent = $(this).children('div[class="col col5 col-last"]').children("p").text();
                            shipingPolicyArr.push(shipping);
                          }

                        });
                        product.shipingPolicy = shipingPolicyArr;
                        if (product.variants.length > 0) {
                          // console.log(product);
                          // product.variants[0].data.forEach(function(ere){
                          // 	console.log(ere);
                          // });
                        }
                        Product.create(product, function(productErr, productObj) {
                          if (productErr) {
                            insertedFailedCount++;
                            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            console.log("Product insertion error : " + productErr);
                            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            return setImmediate(fileDataCallback);
                          } else {
                            insertedCount++;
                            return setImmediate(fileDataCallback);
                          }
                        });
                      }

                    } else {
                      // shiping Policy not available
                      product.shipingPolicy = [];

                      Product.create(product, function(productErr, productObj) {
                        if (productErr) {
                          insertedFailedCount++;
                          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                          console.log("Product insertion error : " + productErr);
                          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                          return setImmediate(fileDataCallback);
                        } else {
                          insertedCount++;
                          return setImmediate(fileDataCallback);
                        }
                      });
                    }
                  });

                } else {
                  // review not available
                  product.reviews = [];

                  request(shipingPolicyUrl, function(shipingPolicyErr, shipingPolicyRes, shipingPolicyBody) {
                    if (!shipingPolicyErr && shipingPolicyRes.statusCode == 200 && shipingPolicyBody) {
                      var $ = cheerio.load(shipingPolicyBody);
                      $.html();

                      var parsedHTML = $.load(JSON.parse(shipingPolicyBody).html);

                      // console.log(parsedHTML("div").children("div").data("region", "policy-subregions").children('div[class="col-group shop-policies-section"]').length);

                      if (parsedHTML("div").children("div").data("region", "policy-subregions") && parsedHTML("div").children("div").data("region", "policy-subregions").children('div[class="col-group shop-policies-section"]').length > 0) {

                        var shipingPolicy = [];

                        parsedHTML("div").children("div").data("region", "policy-subregions").children('div[class="col-group shop-policies-section"]').each(function(i, elem) {

                          var shippingLabel = $(this).children('div[class="col-lg-3 pt-xs-4 pt-lg-3 pl-xs-1 pb-xs-2 pb-lg-6"]').children('span[class="b"]').text();

                          // var shippingContent =
                          // console.log($(this).children('div[class="col-lg-9 pt-lg-3 pb-xs-2 pl-xs-1"]'));

                          // console.log($(this).children('div[class="col-lg-9 pt-lg-3 pb-xs-2 pl-xs-1"]').children('div[class="pl-xs-0 mb-xs-3"]').children('div').attr("id","listing-shipping-estimate").html());


                          console.log($(this).children('div[class="col-lg-9 pt-lg-3 pb-xs-2 pl-xs-1"]').children('div[class="pl-xs-0 mb-xs-3"]').children('div').attr("id", "listing-shipping-estimate").html());

                          // pl-xs-0 text-smaller text-gray-lightest
                          // pl-xs-0 mb-xs-3
                          // listing-shipping-estimate

                          // console.log($(this).children('div[class="col-lg-9 pt-lg-3 pb-xs-2 pl-xs-1"]'));
                        });
                        product.shipingPolicy = shipingPolicy;

                        if (product.variants.length > 0) {
                          // console.log(product);
                          // product.variants[0].data.forEach(function(ere){
                          // 	console.log(ere);
                          // });
                        }



                        Product.create(product, function(productErr, productObj) {
                          if (productErr) {
                            insertedFailedCount++;
                            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            console.log("Product insertion error : " + productErr);
                            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            return setImmediate(fileDataCallback);
                          } else {
                            insertedCount++;
                            return setImmediate(fileDataCallback);
                          }
                        });
                      } else {
                        var shipingPolicyArr = [];
                        console.log("Its ELSEEEEEEEEEEEEE..............");
                        parsedHTML("div").each(function(i, elem) {
                          var shipping = {};
                          var slabel = $(this).children('div[class="col col2"]').children("h2").text();
                          if (slabel) {
                            shipping.label = slabel;
                            shipping.labelContent = $(this).children('div[class="col col5 col-last"]').children("p").text();
                            shipingPolicyArr.push(shipping);
                          }
                        });
                        product.shipingPolicy = shipingPolicyArr;
                        Product.create(product, function(productErr, productObj) {
                          if (productErr) {
                            insertedFailedCount++;
                            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            console.log("Product insertion error : " + productErr);
                            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            return setImmediate(fileDataCallback);
                          } else {
                            insertedCount++;
                            return setImmediate(fileDataCallback);
                          }
                        });
                      }

                    } else {
                      // shiping Policy not available
                      product.shipingPolicy = [];
                      Product.create(product, function(productErr, productObj) {
                        if (productErr) {
                          insertedFailedCount++;
                          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                          console.log("Product insertion error : " + productErr);
                          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                          return setImmediate(fileDataCallback);
                        } else {
                          insertedCount++;
                          return setImmediate(fileDataCallback);
                        }
                      });
                    }
                  });
                }
              });

              // console.log("shipingPolicyUrl");
              // console.log(shipingPolicyUrl);
              // console.log(product);
              // shipingPolicyUrl = "https://www.etsy.com/listing/399757909/stubs/policy";

            } else {
              // When Listing Id is not available
              dataListingIdNotPresentCount++;
              console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
              console.log("dataListingId is not present");
              console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
              return setImmediate(fileDataCallback);
            }

          } else {
            priceNotPresentCount++;
            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            console.log("Either price and currency or currencyValue and currencySymbol is not present");
            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            return setImmediate(fileDataCallback);
          }


        } else {
          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
          console.log("URL Request Error");
          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
          return setImmediate(fileDataCallback);
        }
      });

    } else {
      console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
      console.log("No product detail page url available");
      console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
      return setImmediate(fileDataCallback);
    }
  }, function(fileDataErr) {
    if (fileDataErr) {
      console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
      console.log("Async File Data Error");
      console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
      return setImmediate(fileCallback);
    } else {
      //write to the file

									    productList = Array.from(new Set(productList));

                      // check array length before writing the file

                      var addHeaderToArr = [];
                      productList.forEach(function(element) {
                        addHeaderToArr.push({
                          'title': element.title,
                          'shop_link': element.shopLink,
                          'shop_name': element.shopName,
                          'item_details': element.itemDetails,
                          'currency': element.currency,
                          'price': element.price,
                          'currency_symbol': element.currencySymbol,
                          'currency_value': element.currencyValue,
                          'overview': element.overview,
                          'images': element.images,
                          'main_category': element.mainCategory,
                          'mid_category': element.midCategory,
                          'cat_category': element.catCategory,
                          'data_listing_id': element.dataListingId,
                          'listing_title': element.listingTitle,
                          'detail_page_url': element.detailPageUrl,
                          'url_called': element.urlCalled
                        });
                      });

                      json2csv({
                        data: addHeaderToArr,
                        fields: fields
                      }, function(csvErr, csv) {
                        if (csvErr) {
                          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                          console.log("array to csv conversion error: " + csvErr);
                          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                          return setImmediate(fileCallback);
                        } else {
                          fs.writeFile('./server/resources/products/' + file, csv, function(fileWriteErr) {
                            if (fileWriteErr) {
                              console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                              console.log("File write error:" + fileWriteErr);
                              console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                              return setImmediate(fileCallback);
                            } else {
                              console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                              console.log(file + 'file saved' + "@ " + './server/resources/products/');
                              console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                              return setImmediate(fileCallback);
                            }
                          });
                        }
                      });

      return setImmediate(fileCallback);
    }
  });
*/
};


module.exports.justdial_scraper(function(err, res) {
  if (err) {
    console.log(err);
    process.exit(1);
  } else {
    console.log("------------------------------DONE WITH SCRAPING------------------------------");
    process.exit(1);
  }
});
