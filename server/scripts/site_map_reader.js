var async = require('async');
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var parseString = require('xml2js').parseString;
var Converter = require("csvtojson").Converter;
var converter = new Converter({});
var json2csv = require('json2csv');
var parser = require('xml2json');
var fields = ['url'];

var models = require('../../server/server.js').models;
var Site_map_urls = models.site_map_urls;

/* We can provide as many site as we want in this data object */
var data = {
  LinkedIn: {
    site_url: "https://www.shoppersstop.com",
    site_path: "/sitemap.xml",
    site_name: "LinkedIn"
  },
  codeschool: {
    site_url: "https://www.codeschool.com",
    site_path: "/sitemap.xml",
    site_name: "codeschool"
  },
  flipkart: {
    site_url: "https://www.flipkart.com",
    site_path: "/sitemap.xml",
    site_name: "flipkart"
  }
};



module.exports.site_map_reader = function(site, done) {

  if (site.site_url && site.site_path) {
    var insertedFailedCount = 0;
    var insertedCount = 0;
    var urls_arr = [];
    var siteMapUrl = site.site_url + site.site_path;
    var robotsUrl = site.site_url + "/robots.txt";
    // var dir_path = "sites" + "/" + site.site_name;
    var dir_path = "./server/resources/sites/" + site.site_name;

    console.log(site.site_name);

    request(siteMapUrl, function(err, res, body) {
      if (!err && res.statusCode == 200 && body) {

        parseString(body, {
          explicitArray: false,
          explicitRoot: false
        }, function(err, body_data) {
          if (err) {
            console.log(err);
            return done();
          } else {
console.log(body_data); process.exit(1);
            async.eachSeries(body_data.url, function(resData, resDataCallback) {
              urls_arr.push({
                "url": resData.loc
              });

              Site_map_urls.create({
                "url": resData.loc
              }, function(siteMapErr, siteMapObj) {
                if (siteMapErr) {
                  insertedFailedCount++;
                  console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                  console.log("Product insertion error : " + siteMapErr);
                  console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                  return setImmediate(resDataCallback);
                } else {
                  insertedCount++;
                  return setImmediate(resDataCallback);
                }
              });

            }, function(err, res) {
              if (err) {
                console.log(err);
                return done();
              } else {
                json2csv({
                  data: urls_arr,
                  fields: fields
                }, function(csvErr, csv) {
                  if (csvErr) {
                    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                    console.log("array to csv conversion error: " + csvErr);
                    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                    return setImmediate(done);
                  } else {

                    if (!fs.existsSync(dir_path)) {
                      fs.mkdirSync(dir_path, 0766);

                      if (fs.existsSync(dir_path)) {
                        console.log("Folder created successfully");

                        fs.writeFile(dir_path + '/' + 'site_map.csv', csv, function(fileWriteErr) {
                          if (fileWriteErr) {
                            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            console.log("File write error:" + fileWriteErr);
                            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                            return setImmediate(done);
                          } else {
                            console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                            console.log('site_map.csv file saved' + "@ " + dir_path);
                            console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                            return setImmediate(done);
                          }
                        });
                      }

                    } else {
                      console.log("folder already exists");
                      fs.writeFile(dir_path + '/' + 'site_map.csv', csv, function(fileWriteErr) {
                        if (fileWriteErr) {
                          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                          console.log("File write error:" + fileWriteErr);
                          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                          return setImmediate(done);
                        } else {
                          console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                          console.log('site_map.csv file saved' + "@ " + dir_path);
                          console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                          return setImmediate(done);
                        }
                      });
                    }
                  }
                });
              }
            });
          }
        });
      } else {
        console.log(robotsUrl);
        var siteMapUrlsFetched = [];
        request(robotsUrl, function(err, res, body) {
          if (!err && res.statusCode == 200 && body) {
            var bbmArr = body.split("\n");
            bbmArr.forEach(function(ele, i) {
              if (ele.indexOf("Sitemap:") > -1) {
                var splitData = ele.split("Sitemap:");
                siteMapUrlsFetched.push({
                  "url": splitData[1].trim()
                });
              }
            });

            console.log("************************************");
            json2csv({
              data: siteMapUrlsFetched,
              fields: fields
            }, function(csvErr, csv) {
              if (csvErr) {
                console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                console.log("array to csv conversion error: " + csvErr);
                console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                return setImmediate(done);
              } else {

                if (!fs.existsSync(dir_path)) {
                  fs.mkdirSync(dir_path, 0766);

                  if (fs.existsSync(dir_path)) {
                    console.log("Folder created successfully");

                    fs.writeFile(dir_path + '/' + 'robots.csv', csv, function(fileWriteErr) {
                      if (fileWriteErr) {
                        console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                        console.log("File write error:" + fileWriteErr);
                        console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                        return setImmediate(done);
                      } else {
                        console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                        console.log('robots.csv file saved' + "@ " + dir_path);
                        console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                        return setImmediate(done);
                      }
                    });
                  }

                } else {
                  console.log("folder already exists");
                  fs.writeFile(dir_path + '/' + 'robots.csv', csv, function(fileWriteErr) {
                    if (fileWriteErr) {
                      console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                      console.log("File write error:" + fileWriteErr);
                      console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                      return setImmediate(done);
                    } else {
                      console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                      console.log('robots.csv file saved' + "@ " + dir_path);
                      console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                      return setImmediate(done);
                    }
                  });
                }
              }
            });

          } else {
            console.log("Inside ELSEEEEE");
            return done();
          }
        });
      }
    });

  } else {
    console.log("Provide site url and site path to proceed with sitemap reading");
    return done();
  }
};

/* site map function calling */
module.exports.site_map_reader(data.LinkedIn, function(err) {
  if (err) {
    console.log(err);
    process.exit(1);
  } else {
    console.log("------------------------------DONE WITH SCRAPING------------------------------");
    process.exit(1);
  }
});
