var request = require("request");
var async = require("async");
var fs = require("fs");
var path = require("path");
var parseString = require('xml2js').parseString;
var Converter = require("csvtojson").Converter;
var converter = new Converter({});
var models = require('../../server/server.js').models;


module.exports.robots_sitemap_reader = function(site, callback) {
  if (site && site !== undefined && site !== "") {

    var filePath = "./server/resources/sites/" + site + "/robots.csv";
    if (fs.existsSync(filePath)) {
      var con = new Converter({});
      fs.createReadStream(filePath).pipe(con);
      con.on("end_parsed", function(categoriesJsonArray) {
        console.log("converted...............");

        if (categoriesJsonArray && typeof categoriesJsonArray !== 'undefined' && categoriesJsonArray instanceof Array && categoriesJsonArray.length > 0) {

          var skip_records = 0;
          var insertCount = 0;
          var failedInsertCount = 0;
          var find_response = {};
					async.eachSeries(categoriesJsonArray, function(record, recordCallback) {
							console.log(record['url']);
					    if (path.extname(record['url']) == ".xml") {
								console.log("XML file");

							    request(record['url'], function(err, res, body) {
							      if (!err && res.statusCode == 200 && body) {
											parseString(body, {
												explicitArray: false,
												explicitRoot: false
											}, function(err, body_data) {
												if (err) {
													console.log(err);
													return recordCallback();
												} else {
													console.log(body_data.sitemap.loc);

// console.log(path.extname(body_data.sitemap.loc));
													if (path.extname(body_data.sitemap.loc) == ".gz") {
														request(body_data.sitemap.loc, function(err, res, body) {
															if (!err && res.statusCode == 200 && body) {
																console.log(body)
																console.log("In IFFFFFFF")
																return recordCallback();
															}else{
																console.log("In Elseeee");
																return recordCallback();
															}
														});
													}




													// async.eachSeries(body_data.sitemap, function(resData, resDataCallback) {
													// 	urls_arr.push({
													// 		"url": resData.loc
													// 	});
													// }, function(err, res) {
													// 	if (err) {
													// 		console.log(err);
													// 		return recordCallback();
													// 	} else {
													// 		console.log(res);
													// 		return recordCallback();
													// 	}
													// });
												}
											});
											console.log("If case");
										}else{
											console.log("else case");
										}
									});

								// return setImmediate(recordCallback);
							}else{
								console.log("NOT an XML file");
								return setImmediate(recordCallback);
							}

					}, function(recordErr, recordRes) {
						if (recordErr) {
							console.log("Affiliate products async error : " + recordErr);
							return callback();
						} else {
							console.log("skip_records is = " + skip_records);
							console.log("Loop ended successfully");
							return callback();
						}
					});
        } else {
          return callback(new Error("Mapped category file is empty"));
        }
      });
    } else {
      return callback(new Error("File does not exist."));
    }
  } else {
    return callback(new Error("Site name not provided"));
  }
};


module.exports.robots_sitemap_reader("flipkart", function(err) {
  if (err) {
    console.log("something went wrong : " + err);
    process.exit(1);
  } else {
    console.log("All Ok!");
    process.exit(1);
  }
});
