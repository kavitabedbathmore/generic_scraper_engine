var async = require('async');
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var parseString = require('xml2js').parseString;
var Converter = require("csvtojson").Converter;
var converter = new Converter({});
var json2csv = require('json2csv');
var path = require("path");
var superagent = require('superagent');
var fields = ['title'];

var models = require('../../server/server.js').models;
var JustdialServiceProvider = models.justdial_service_provider;
// srchpagination
// https://www.justdial.com/Mumbai/Plumbers/ct-134859/page-50

// var cities = ["Ahmedabad", "Bangalore", "Chandigarh", "Chennai", "Coimbatore", "Delhi", "Goa", "Gurgaon", "Hyderabad", "Indore", "Jaipur", "Kolkata", "Mumbai", "Noida", "Pune"]
var cities = ["Mumbai"]

module.exports.justdial_scraper = function(done) {

	var productList = [];
	var totalPages = 0;
	var page = 0;
	var host = 'https://www.justdial.com';
	var category = 'Plumbers/ct-134859';
	var find_response = 1;
	// var query_str = "?sort=pop_desc";
	// query_str = query_str + "&fsearch=switches";

async.eachSeries(cities, function(city, cityCallback) {

var cityUrl = host + "/" + city;
cityUrl = cityUrl + "/" + category;
console.log("Inside city  ==  " + city);
	async.whilst(function() {
			console.log("inside whilst page =" + page);
			if (find_response > 0 || page == 0) {
				return true;
			} else {
				return false;
			}
		},
		function(next) {
			find_response = 0;
			var reqUrl = cityUrl + "/" + "page-" + page;

			console.log(reqUrl);
			console.log("PAGE---------------------------------------------------------------- ", page);
// reqUrl = "https://www.justdial.com/Mumbai/Plumbers/ct-134859/page-29";
				page++;
				superagent
				.get(reqUrl)
				.set('Accept', 'application/json')
				.end(function(err, res){
					var $ = cheerio.load(res.text);

					var product = {};
					product.listingCategory = "Plumbers";
					console.log("inside request");

					$("div[class='result_page']").children("div").attr("class","col-md-12 rsp").next().children("div").attr("class","right-ct-container col-md-10 col-sm-10  padding0").children("div[id='tab_block']").children("div[class='tabsM']").children("section[class='rslwrp ']").children("div[id='tab-5']").children("ul[class='rsl col-md-12 padding0']").filter(function() {

								find_response = $(this).children("li[class='cntanr']").length;

								$(this).children("li[class='cntanr']").each(function(i, elem) {
console.log("Rotation --------------------------------------------------------", i);
								product.listingTitle = $(this).children("section[class='jbbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("h4[class='store-name']").children("span[class='jcn']").children("a").text();

								if(!product.listingTitle){
									product.listingTitle = $(this).children("section[class='jgbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("h4[class='store-name']").children("span[class='jcn']").children("a").text();
								}


								product.listingStoreName = $(this).children("section[class='jbbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("h4[class='store-name']").children("span[class='jcn']").children("a").text();

								if(product.listingStoreName == "" || product.listingStoreName == undefined){
									product.listingStoreName = $(this).children("section[class='jgbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("h4[class='store-name']").children("span[class='jcn']").children("a").text();
								}

								product.detailPageUrl = $(this).attr("data-href");
								product.gallery = $(this).children("section[class='jbbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("div").attr("class","col-sm-4 col-xs-12 padding0 imgSec card_imgwrap").children("div[class='thumb_img']").children("a").first().attr("href");

								if(product.gallery == "" || product.gallery == undefined){
									product.gallery = $(this).children("section[class='jgbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("div").attr("class","col-sm-4 col-xs-12 padding0 imgSec card_imgwrap").children("div[class='thumb_img']").children("a").first().attr("href");
								}

								product.listingRating = $(this).children("section[class='jbbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("ul[class='est-info phone']").children("li").first().children("span[class='exrt_count']").text();

								if(!product.listingRating){
									product.listingRating = $(this).children("section[class='jgbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("ul[class='est-info phone']").children("li").first().children("span[class='exrt_count']").text();
								}

								product.no_of_votes = $(this).children("section[class='jbbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("ul[class='est-info phone']").children("li").first().next().children("a").children("span[class='rt_count']").text().replace(/\t/g, "").replace(/Votes/g,"").trim();

								if(!product.no_of_votes){
									product.no_of_votes = $(this).children("section[class='jgbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("ul[class='est-info phone']").children("li").first().next().children("a").children("span[class='rt_count']").text().replace(/\t/g, "").replace(/Votes/g,"").trim();
								}

								product.lisitngNoOfVotes = $(this).children("section[class='jbbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("p[class='newrtings ']").children("a[class='rating_div']").children("span[class='rt_count']").text().replace(/\t/g, "").replace(/Votes/g,"").trim();

								if(!product.lisitngNoOfVotes){
									product.lisitngNoOfVotes = $(this).children("section[class='jgbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("p[class='newrtings ']").children("a[class='rating_div']").children("span[class='rt_count']").text().replace(/\t/g, "").replace(/Votes/g,"").trim();

								}

								product.listingPhoneNumber =$(this).children("section[class='jbbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("p[class='contact-info ']").children("span").children("a").children("b").text();

								if(!product.listingPhoneNumber){

									product.listingPhoneNumber =$(this).children("section[class='jgbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("p[class='contact-info ']").children("span").children("a").text();
								}

								product.listingAddress = $(this).children("section[class='jbbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("p[class='address-info tme_adrssec']").children("span[class='desk-add jaddt']").children("a").children("span").attr("class","mrehover dn").text().replace(/\t/g, "").replace(/\n/g,"").trim();

								if(!product.listingAddress){

									product.listingAddress = $(this).children("section[class='jgbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("section[class='jcar']").children("div").attr("class","col-sm-5 col-xs-8 store-details sp-detail paddingR0").children("p[class='address-info tme_adrssec']").children("span[class='desk-add jaddt']").children("a").children("span").attr("class","mrehover dn").text().replace(/\t/g, "").replace(/\n/g,"").trim();
								}

								// console.log( $(this).children("section[class='jbbg']").children("div").attr("class","col-md-12 col-xs-12  colsp").children("div").attr("class","col-sm-4 col-xs-12 padding0 imgSec card_imgwrap").children("div[class='thumb_img']").children("a").attr("data-original"));


if(product.listingTitle && product.listingStoreName && product.detailPageUrl && product.gallery && product.listingRating && product.no_of_votes && product.lisitngNoOfVotes && product.listingPhoneNumber && product.listingAddress){

	// console.log("------------------------------------------------------ALL OK")
}else{
console.log(product);

}


									JustdialServiceProvider.create(product, function(errObj,resObj){
										if(errObj){
											console.log("Product insertion error :", errObj);
										}else{
											console.log("Product inserted successfully");
										}
									});

								});
							});
							return next();
				});

		},
		function(err) {
			if (err) {
				console.log("Pagination error : " + err);
				return setImmediate(cityCallback);
			} else {
				console.log("All the pages are traversed successfully");
				return setImmediate(cityCallback);
			}
		});
}, function(recordErr, recordRes) {
	if (recordErr) {
		console.log("City async error : " + recordErr);
		return setImmediate(done);
	} else {
		console.log("All the cities are done");
		// console.log("Loop ended successfully");
		return setImmediate(done);
	}
});
};


module.exports.justdial_scraper(function(err, res) {
  if (err) {
    console.log(err);
    process.exit(1);
  } else {
    console.log("------------------------------DONE WITH SCRAPING------------------------------");
    process.exit(1);
  }
});
