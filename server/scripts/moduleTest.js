var htmlToJson = require('html-to-json');
var async = require('async');
var url = require('url');
var homeUrl = url.parse('https://www.etsy.com/in-en/c/bags-and-purses/electronics-cases/laptop-bags?ref=catnav-2938');

var parseLinks = htmlToJson.createParser(['a[href]', {
  'text': function($a) {
    return $a.text().trim();
  },
  'href': function($a) {
    return url.resolve(homeUrl, $a.attr('href'));
  },
  'headings': function() {
    return this.get('href').then(function(href) {
      var parsedUrl = url.parse(href);

      // Only bother Prolific's server for this example
      if (parsedUrl.protocol === 'http:' && parsedUrl.hostname === homeUrl.hostname) {
        return parseHeadings.request(href);
      } else {
        return null;
      }
    });
  }
}]);


var parseHeadings = htmlToJson.createParser(['h1,h2,h3,h4,h5,h6', function($hx) {
  return $hx.text().trim();
}]);

// parseLinks.request(url.format(homeUrl)).done(function (links) {
//   console.log(links);
// }, function (err) {
//   throw err;
// });

var dataObj = {
  "productBlockElement": "div",
  "productBlockClass": "block-grid-item",
  "productBlockId": "",
  "content": [{
    "element": "div",
    "class": "block-grid-item",
    "id": "",
    "attr": "data-palette-listing-id",
    "text": false,
    "html": false,
    "name": "id"
  }, {
    "element": "a",
    "class": "buyer-card",
    "id": "",
    "attr": "href",
		"text": false,
		"html": false,
    "name": "listingImage"
  },
	{
    "element": "div",
    "class": "card-title",
    "id": "",
    "attr": "",
		"text": true,
		"html": false,
    "name": "listingTitle"
  },
	{
		"element": "span",
		"class": "currency-symbol",
		"id": "",
		"attr": "",
		"text": true,
		"html": false,
		"name": "currency"
	},
	{
		"element": "span",
		"class": "currency",
		"id": "",
		"attr": "",
		"text": true,
		"html": false,
		"name": "price"
	}]
};


function getProductDetails(id, callback) {
  return htmlToJson.request({
    uri: 'http://store.prolificinteractive.com/products/' + id
  }, {
    'id': function($doc) {
      return $doc.find('#product-details').attr('data-id');
    },
    'colors': ['.color', {
      'id': function($color) {
        return $color.attr('data-id');
      },
      'hex': function($color) {
        return $color.css('background-color');
      }
    }]
  }, callback);
}

function getProducts(dataObj, callback) {

  if (dataObj) {
    var productBlock = "";
    var productClass = "";
    var productId = "";
    var productElement = "";

    if (dataObj.productBlockClass !== "" && dataObj.productBlockClass !== undefined) {
      productBlock = "." + dataObj.productBlockClass;
      productClass = "." + dataObj.productBlockClass;
    } else if (dataObj.productBlockId !== "" && dataObj.productBlockId !== undefined) {
      productBlock = "#" + dataObj.productBlockId;
      productId = "#" + dataObj.productBlockId;
    } else {
      productBlock = dataObj.productBlockElement;
      productElement = dataObj.productBlockElement;
    }

    if (productBlock !== "" && productBlock !== undefined) {

      var contentObj = {};
      if (dataObj.content && typeof dataObj.content !== 'undefined' && dataObj.content instanceof Array && dataObj.content.length > 0) {

        var content = dataObj.content;
        async.eachSeries(content, function(contentItem, contentItemCallback) {

          if (contentItem.name) {  // contentItem.attr
            // contentItem.element contentItem.class	contentItem.id
            var contentBlock = "";
            if (contentItem.class !== "" && contentItem.class !== undefined) {
              contentBlock = "." + contentItem.class;

              if (productClass && (contentBlock == productClass || contentBlock.indexOf(productClass) > -1 || productClass.indexOf(contentBlock) > -1)) {

									if(contentItem.attr){
									    contentObj[contentItem.name] = function($product) {
									      return $product.attr(contentItem.attr);
									    };
									}else{
										if(contentItem.text){
											contentObj[contentItem.name] = function($product) {
												return $product.text();
											};
										}else if(contentItem.html){
											contentObj[contentItem.name] = function($product) {
									      return $product.html();
									    };
										}else{
											return contentItemCallback(new Error("Source property to be scraped is not given"));
										}
									}

              } else {

								if(contentItem.attr){
										contentObj[contentItem.name] = function($product) {
											return $product.find(contentBlock).attr(contentItem.attr);
										};
								}else{
									if(contentItem.text){
										contentObj[contentItem.name] = function($product) {
											return $product.find(contentBlock).text();
										};
									}else if(contentItem.html){
										contentObj[contentItem.name] = function($product) {
											return $product.find(contentBlock).html();
										};
									}else{
										return contentItemCallback(new Error("Source property to be scraped is not given"));
									}
								}

              }

            } else if (contentItem.id !== "" && contentItem.id !== undefined) {
              contentBlock = "#" + contentItem.id;

              if (productId && (contentBlock == productId || contentBlock.indexOf(productId) > -1 || productId.indexOf(contentBlock) > -1)) {

								if(contentItem.attr){
									contentObj[contentItem.name] = function($product) {
										return $product.attr(contentItem.attr);
									};
								}else{
									if(contentItem.text){
										contentObj[contentItem.name] = function($product) {
		                  return $product.text();
		                };
									}else if(contentItem.html){
										contentObj[contentItem.name] = function($product) {
		                  return $product.html();
		                };
									}else{
										return contentItemCallback(new Error("Source property to be scraped is not given"));
									}
								}

              } else {

									if(contentItem.attr){
										contentObj[contentItem.name] = function($product) {
		                  return $product.find(contentBlock).attr(contentItem.attr);
		                };
									}else{
										if(contentItem.text){
											contentObj[contentItem.name] = function($product) {
			                  return $product.find(contentBlock).text();
			                };
										}else if(contentItem.html){
											contentObj[contentItem.name] = function($product) {
			                  return $product.find(contentBlock).html();
			                };
										}else{
											return contentItemCallback(new Error("Source property to be scraped is not given"));
										}
									}
              }
            } else {
              contentBlock = contentItem.element;

							if(contentItem.attr){
								contentObj[contentItem.name] = function($product) {
									return $product.find(contentBlock).attr(contentItem.attr);
								};
							}else{
								if(contentItem.text){
									contentObj[contentItem.name] = function($product) {
										return $product.find(contentBlock).text();
									};
								}else if(contentItem.html){
									contentObj[contentItem.name] = function($product) {
										return $product.find(contentBlock).html();
									};
								}else{
									return contentItemCallback(new Error("Source property to be scraped is not given"));
								}
							}
            }
            // class can be same id cant be and element can also be same

            return contentItemCallback();
          } else {
            return contentItemCallback(new Error("Item attribute to be scraped is not provided or Item attribute presentation name is not provided"));
          }


        }, function(err) {
          if (err) {
            console.log("async error");
            return callback("Product block properties to be scraped are not provided is the correct format");
          } else {
            console.log("res area");

            return htmlToJson.request({
              uri: 'https://www.etsy.com/in-en/c/bags-and-purses/electronics-cases/laptop-bags?ref=catnav-2938'
            }, [productBlock, contentObj], callback);

          }
        });
      } else {
        return callback("Product block properties to be scraped are not provided");
      }

    } else {
      return callback("Product block not provided");
    }
  } else {
    return callback("Listing data properties not provided");
  }

  /*
  return htmlToJson.request({
  	uri: 'https://www.etsy.com/in-en/c/bags-and-purses/electronics-cases/laptop-bags?ref=catnav-2938'
  }, ['.block-grid-item', {
  	'id': function ($product) {
  		return $product.attr('data-palette-listing-id');
  	},
  	'listingImage': function ($product) {
  		return $product.find('.buyer-card').attr('href');
  	}
  	'colors': function ($product) {
  		// This is where we use a promise to get the colors asynchronously
  		return this
  			.get('id')
  			.then(function (id) {
  				return getProductDetails(id).get('colors');
  			});
  	}
  }], callback);
  */

}

// getProducts(dataObj, function(err, res) {
//   console.log(err);
//   if (err) {
//     console.log("there is some error");
//     process.exit(1)
//   } else {
//
//     console.log(res);
//
//     process.exit(1)
//   }
// });



function getProductDetailsLatest (detailPageUrl, callback) {
  return htmlToJson.request({
    uri: detailPageUrl
  }, {
    'id': function ($doc) {
      return $doc.find('h1').text();
    },
    'colors': function($doc){
			return $doc.find("#item-overview").text();
		}
  }, callback);
}

function getProductsLatest (callback) {
  return htmlToJson.request({
    uri: 'https://www.etsy.com/in-en/c/bags-and-purses/electronics-cases/laptop-bags?ref=catnav-2938'
  }, ['.block-grid-item', {
    'detailPage': function ($product) {
      return $product.find('.buyer-card').attr('href');
    },
    'colors': function ($product) {
			console.log($product.detailPage);
      // This is where we use a promise to get the colors asynchronously
      return this.get('detailPage').then(function (detailPageUrl) {
          return getProductDetails(detailPageUrl).get('id');
        });
      // return this
      //   .get('id')
      //   .then(function (id) {
      //     return getProductDetails(id).get('colors');
      //   });
    }
  }], callback);
}


getProductsLatest(function(err,res){
	if(err){
		console.log(err);
		process.exit(1)
	}else{
		console.log(res);
		process.exit(1)
	}
});
