	var server = require('../../server/server');
	var ineed = require('ineed');
	var async = require('async');
	var request = require('request');
	var fs = require('fs');
	var path = require('path');
	var json2csv = require('json2csv');
	var Converter = require("csvtojson").Converter;
	var converter = new Converter({});

	// var models = app.models;
	// var Site_map_urls = models.site_map_urls;


	var urls = [];
	var fields = ["href", "text"];

	var blackListedItems = ["STORES", "SHIP TO", "New", "Christmas", "Hanukkah", "New Year\'s Eve", "Gifts", "Toy Shop", "Blog", "Ideas & Advice", "Wedding Registry", "Sign In", "Sign Up", "Registry Benefits", "Registry Bonus Gifts", "Registry Checklist", "Start Now", "Top 6 Reasons to Register", "Private Registry Events", "Sign in", "View Cart", "Checkout", "Site Index", "Terms of Use", "Privacy Policy", "View Desktop Site", "Customer Service", "Careers", "Feedback", "Returns/Exchanges", "Customer Service", "Order Tracking", "About Us", "Contact Us", "Free", "All", "Special Offers", "off", "Holiday", "Limited Time Offers", "Rated", "Top", "Use", "How to", "Top Rated Outdoor", "Sale", "New Clearance", "Free Shipping", "All Free Shipping", "Log In", "Signup", "24x7 Customer Care", "Download App", "Advertise", "Payments", "FAQ", "Online Shopping", "Returns Policy", "Security", "Privacy", "Offers", "Women Store", "Men Store", "Google Pixel", "Track Order", "Gift Card", "Press"];


	module.exports = function(SiteRegistration) {
	  SiteRegistration.validatesPresenceOf('site_name');
	  SiteRegistration.validatesPresenceOf('site_url');
	  SiteRegistration.validatesUniquenessOf('site_name', {
	    message: 'Site name is not unique'
	  });
	  SiteRegistration.validatesUniquenessOf('site_url', {
	    message: 'Site URL is not unique'
	  });

	  // SiteRegistration.validatesInclusionOf('type', { in : ['comment', 'like', 'post', 'space', 'offer', 'share']
	  // });
	  // SiteRegistration.validatesPresenceOf('pageId');
	  // var siteSchema = {
	  // 		  "site_name": { "type": "string", "required": true },
	  // 		  "site_url":  { "type": "string", "required": true }
	  // 		};
	  // var Product = Model.extend('product', siteSchema);

	  SiteRegistration.observe('access', function includeRelations(ctx, next) {
	    var filter = ctx.query;
	    if (!filter) {
	      ctx.query = filter = {};
	    }
	    if (!filter.order) {
	      filter.order = ["created DESC"];
	    }
	    // if (!filter.include) {
	    //   filter.include = ["page"];
	    // }

	    next();
	  });

	  // SiteRegistration.validateUniquenessOf('SiteRegistration', { scopedTo: ['_id'] });

	  SiteRegistration.createSiteMapCSV = function(data, fields, site_name, callback) {
	    if (data && fields && site_name) {
	      var dir_path = "./server/resources/sites/" + site_name;
	      var milliseconds = new Date().getTime();
	      json2csv({
	        data: data,
	        fields: fields
	      }, function(csvErr, csv) {
	        if (csvErr) {
	          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	          console.log("array to csv conversion error: " + csvErr);
	          console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	          return callback(csvErr);
	        } else {

	          if (!fs.existsSync(dir_path)) {
	            fs.mkdirSync(dir_path, 0766);

	            if (fs.existsSync(dir_path)) {
	              console.log("Folder created successfully");
	              fs.writeFile(dir_path + '/' + 'sitemap.csv', csv, function(fileWriteErr) {
	                if (fileWriteErr) {
	                  console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	                  console.log("File write error:" + fileWriteErr);
	                  console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	                  return callback(fileWriteErr);
	                } else {
	                  console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	                  console.log('sitemap.csv file saved' + "@ " + dir_path);
	                  console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	                  return setImmediate(callback);
	                }
	              });
	            }

	          } else {
	            console.log("Folder already exists");

	            fs.writeFile(dir_path + '/' + 'sitemap.csv', csv, function(fileWriteErr) {
	              if (fileWriteErr) {
	                console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	                console.log("File write error:" + fileWriteErr);
	                console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	                return callback(fileWriteErr);
	              } else {
	                console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	                console.log('sitemap.csv file saved' + "@ " + dir_path);
	                console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	                var resFilePath = dir_path + '/' + 'sitemap.csv';
	                resFilePath = resFilePath.toString();
	                return callback(null, resFilePath);
	              }
	            });
	          }
	        }
	      });
	    } else {
	      console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	      console.log("Not all Arguments passed to the function");
	      console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	      return callback(new Error('Arguments not passed'));
	    }
	  };

	  SiteRegistration.observe('after save', function(ctx, next) {
	    if (ctx.isNewInstance) {
	      if (ctx.instance) {
	        // console.log(ctx.where);
	        // console.log(ctx.hookState);
	        console.log(ctx.instance);
	        ctx.instance.created = new Date();
	        var site_url = ctx.instance.site_url;
	        var site_name = ctx.instance.site_name;
	        if (site_url) {
	          request(site_url, function(err, res, body) {
	            if (!err && res.statusCode == 200 && body) {
	              ineed.collect.hyperlinks.from(site_url,
	                function(err, response, result) {
	                  if (err) {
	                    return next(new Error("Sitemap could not be generated please check whether valid site url provided or not!"));
	                  } else {

	                    var hyperlinks = result.hyperlinks;
	                    if (hyperlinks && typeof hyperlinks !== 'undefined' && hyperlinks instanceof Array && hyperlinks.length > 0) {

	                      hyperlinks.forEach(function(item, index) {
	                        item.text = item.text.replace(/\n/g, "").trim();
	                        if (item.text && (blackListedItems.indexOf(item.text) == -1)) {
	                          if (item.text.indexOf("All") == -1 || item.text.indexOf("Free") == -1 || item.text.indexOf("%") == -1 || item.text.indexOf("Offers") == -1 || item.text.indexOf("Sale") == -1) {
	                            // if(item.href.indexOf("tel") > -1 && item.href.indexOf("mailto") > -1)
	                            if ("tel".indexOf(item.href) > -1 || "mailto".indexOf(item.href) > -1) {
	                              console.log("yes matched");
	                            } else {
	                              urls.push(item);
	                            }
	                          }
	                        }
	                      });

	                      if (urls.length > 0) {
	                        SiteRegistration.createSiteMapCSV(urls, fields, site_name, function(err, res) {
	                          console.log("new function")
	                          if (err) {
	                            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	                            console.log("function createSiteMapCSV ended with error: " + err);
	                            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
	                            return next(new Error("CSV file generation error occured!"));
	                          } else {
	                            console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	                            console.log("sitemap.csv created successfully for the site " + site_name);
	                            console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

															var newUrls = [];
															urls.forEach(function(item,index){
																var urlObj = {}
																urlObj.url = item.href;
																urlObj.label = item.text.replace(/\n/g, "").trim(); 	//.replace(/<[a-z]>/g,
																urlObj.siteRegistrationId = ctx.instance.id; 					//.replace(/<[a-z]>/g,
																newUrls.push(urlObj);
															});

															console.log(hyperlinks.length)
															console.log(urls.length)
															console.log(newUrls.length)
															console.log(newUrls[0]);

															server.models.site_map_urls.create(newUrls, function(err, resObj) {
																if (err) {
																	console.log(err);
																	return next();
																} else {
																	return next();
																}
															});
	                          }
	                        });
	                      } else {
	                        return next(new Error("URL's not found for this site"));
	                      }
	                    } else {
	                      return next(new Error("URL's not found for this site"));
	                    }
	                  }
	                });
	            } else {
	              return next(new Error("site URL is not valid"));
	            }
	          });

	        } else {
	          return next(new Error("Provide site URL"));
	        }

	      } else {
	        console.log("ctx.instance instance is empty");
	        next();
	      }
	    } else {
	      console.log("ctx.isNewInstance is not a new instance");
	      next();
	    }
	  });
	};
